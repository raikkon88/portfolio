<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'portfolio_DB');

/** MySQL database username */
define('DB_USER', 'portfolio_user');

/** MySQL database password */
define('DB_PASSWORD', 'portfolio/000');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@p*#f]g+J*aO<s,G38sc#BsH>~h3iMBr5QOCJKgIcm-+s hB]8+:-UL;.GNZ2k{3');
define('SECURE_AUTH_KEY',  '?VVlYR`?;pu$,U,>|+gJ|]D,CXQ[F$1d!fAr%JMQ15vw#2?B8lkZi|=@-P]u&`U+');
define('LOGGED_IN_KEY',    ')+hosA:i*/S+qT)np,k-;ehA=~0)?`L,F3PHK.hp9KE>vfih $2mTcibh]yC/r$U');
define('NONCE_KEY',        ')J-m<vt&a#|$?9LKr^zlR-R&7B8kixb|Wij77&?)D~u9,UYD({^PAG%wcSWzhYoq');
define('AUTH_SALT',        'U53>tS(++=.@5kUibO`*n_V!LFPsU~]6V}my5~m+^{Q2/?VW0!,r,jk7qNql7iml');
define('SECURE_AUTH_SALT', ',StgKs>-C$.Y~E6hZKJj!CW,5cWb[| OVo}|H^_`UkD(Rg,E[hw^wKzu,.1L1U+A');
define('LOGGED_IN_SALT',   'v M_?c-XLQwi|5_+mq-1D<?`Yz Rf.pUiXG}b)F![/hvn]HZ6`*H4QhzVg]3j%e{');
define('NONCE_SALT',       '{%N9_Acv`w- UQX5Q4HWg3tjqUc2e72([svWv*B0wtz+LE$D%Rc6|EL}7L,-$}VH');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
